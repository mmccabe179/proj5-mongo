Author: Maura McCabe

Proj5-Mongo implements an ACP brevet control times based on the Randounneurs USA guidelines (https://rusa.org/pages/acp-brevet-control-times-calculator).

acp_times.py does the calculations for the opening and closing times of each brevet distance based on an input (the control brevet distance)

app.py creates the interaction between calc.html and acp_times.py in order to correctly display requested data on the server. 

calc.html using javascript and ajax to update the html page without reloading. The information is displayed through jquery. 

the submit button submits the inputted times into a mongodb database while the display button displays that information in a corresponding html page.

If no entry is inputted an error screen will show.

If the entry inputted is greater than the brevet distance an error will show.

If the last entry inputted is not equal to the brevet distance, an error will show.

The entire program calculates opening and closing times of brevet distances based on control distances entered, start time, and start date. 


